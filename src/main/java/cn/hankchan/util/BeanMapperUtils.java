package cn.hankchan.util;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * @author hankChan
 *         2017/12/26 0026.
 */
public class BeanMapperUtils {

    private static MapperFactory mapperFactory;

    static {
        mapperFactory = new DefaultMapperFactory.Builder().build();
    }

    /**
     * 转换源对象中对应的属性值到新的Bean对象中（使用Orika的实现方式，性能更优）
     * @param src 源对象
     * @param targetType 结果对象类型
     * @param <T> 泛型
     * @return 新的结果对象
     */
    public static <T> T mapperFast(Object src, Class<T> targetType) {
        if(null == src) {
            return null;
        }
        return mapperFactory.getMapperFacade().map(src, targetType);
    }
}
